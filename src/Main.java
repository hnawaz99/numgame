//package elpuig.xeill.net;

import java.util.Random;
import java.util.Scanner;
import java.lang.System;


public class Main{
	public static void main(String[] args){
		System.out.println("Hello and welcome to my number guessing game.");
		System.out.println("Pick a number: ");


		Scanner inputnum = new Scanner(System.in);
		int maxnum;
		maxnum = inputnum.nextInt();

		Random rand = new Random();
		int number = rand.nextInt(maxnum);
		int tries = 0;
		Scanner input = new Scanner(System.in);
		int guess;
		boolean win = false;

		while (win == false){
			System.out.println("==============================================");

			System.out.println("Guess a number between 1 and "+ maxnum +": ");

			System.out.println("==============================================");

			guess = input.nextInt();
			tries++;

			if (guess == number){
				win = true;
			}

			else if(guess < number){
				System.out.println("==============================================");
				System.out.println("Number is too low, Think Bigger Man, try again");
				System.out.println("==============================================");
			}

			else if(guess > number){
				System.out.println("==============================================");
				System.out.println("Number is too high, Go For a Small Number, try again");
				System.out.println("==============================================");
			}
		}

		Result.showResult(tries);

	}
}
